define([], function () {
    require.config({
    // summernote编辑器
    paths: {
        summernote     : "summernote/plugins/summernote/summernote.min",
    },
    shim: {
        // summernote
        summernote:{
            deps: [
                'css!/static/summernote/plugins/summernote/summernote.min.css',
                'css!/static/summernote/plugins/summernote/bootstrap.min.css',
                '/static/summernote/plugins/summernote/bootstrap.min.js',
            ],
            exports: 'summernote',
        },
    },
});
require(['form','upload'],function (Form,Upload){
    Form.events.bindevent = function (form){
        let summernoteEditor = {
            init: {
                config: {
                },
            },
            events: {},
            api: {
                /**
                 * summernote  编辑器
                 */
                editor:function () {
                    var editor = document.querySelectorAll('*[lay-filter="editor"]')
                    if (editor.length > 0) {
                        require(['summernote'],function (summernote){
                            console.log(summernote)
                            $.each(editor, function (i, v) {
                                var _t = $(this);
                                var id = _t.attr('id');
                                var name =_t.attr('name');
                                $('#'+id).summernote({
                                    placeholder: 'Hello FunAdmin',
                                    tabsize: 2,
                                    height: _t.data('height')|| 400,
                                    toolbar: [
                                        ['style', ['style']],
                                        ['font', ['bold', 'underline', 'clear']],
                                        ['color', ['color']],
                                        ['para', ['ul', 'ol', 'paragraph']],
                                        ['table', ['table']],
                                        ['insert', ['link', 'picture', 'video']],
                                        ['view', ['fullscreen', 'codeview', 'help']]
                                    ],
                                    callbacks: {
                                        onChange:function(contents,$editable){
                                            if(!_t.find('textarea[name="'+name+'"]').length>0){
                                                var textarea = '<textarea value="'+Config.formData[name]+'" name="'+name+'">' +
                                                    Config.formData[name]+ '</textarea>';
                                                _t.append(textarea);
                                            }
                                            $('textarea[name="'+name+'"]').val(contents)
                                        },
                                        onInit: function () {
                                        },
                                        onImageUpload: function (files) {
                                            //上传图片到服务器，使用了formData对象，至于兼容性...据说对低版本IE不太友好
                                            var formData = new FormData();
                                            formData.append('file', files[0]);
                                            $.ajax({
                                                url: Fun.url(Upload.init.requests.upload_url),//后台文件上传接口
                                                type: 'POST',
                                                data: formData,
                                                processData: false,
                                                contentType: false,
                                                success: function (data) {
                                                    if(data.url){
                                                        $('#'+id).summernote('insertImage', data.url, 'img');
                                                    }else{
                                                        Fun.toastr.error(__('upload failed'));
                                                    }
                                                }
                                            });
                                        }
                                    },
                                })
                                $('#'+id).summernote('code',Config.formData[name]);
                            })
                        })
                    }
                },
            },
        };
        return summernoteEditor.api.editor();
    }
})

});